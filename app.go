package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/midoryaga13/go_base_api/config"
	"gitlab.com/midoryaga13/go_base_api/controller"
	"gitlab.com/midoryaga13/go_base_api/middleware"
	"gitlab.com/midoryaga13/go_base_api/repository"
	"gitlab.com/midoryaga13/go_base_api/service"
	"gorm.io/gorm"

	_ "github.com/pdrum/swagger-automation/docs" // This line is necessary for go-swagger to find your docs!
)

var (
	db             *gorm.DB                  = config.SetDbConn()
	userRepo       repository.UserRepository = repository.NewUserRepo(db)
	bookRepo       repository.BookRepository = repository.NewBookRepo(db)
	jwtService     service.JWTService        = service.NewJWTService()
	userService    service.UserService       = service.NewUserService(userRepo)
	authService    service.AuthService       = service.NewAuthService(userRepo)
	productService service.BookService       = service.NewBookService(bookRepo)
	authController controller.AuthHandler    = controller.NewAuthHandler(authService, jwtService, userService)
	bookController controller.BookHandler    = controller.NewBookHandler(productService, jwtService)
)

func main() {
	defer config.CloseDbConn(db)
	server := gin.Default()

	authRoutes := server.Group("v1/api/auth")
	{
		authRoutes.POST("/login", authController.Login)
		authRoutes.POST("/register", authController.Register)
	}

	bookRootes := server.Group("v1/api/product", middleware.AuthorizeJWT(jwtService))
	{
		bookRootes.GET("/", bookController.All)
		bookRootes.POST("/", bookController.CreateBook)
		bookRootes.GET("/:id", bookController.FindOneBookByID)
		bookRootes.PUT("/:id", bookController.UpdateBook)
		bookRootes.DELETE("/:id", bookController.DeleteBook)
	}

	server.Run(":7000")

}
