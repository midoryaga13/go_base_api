package controller

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/midoryaga13/go_base_api/dto"
	"gitlab.com/midoryaga13/go_base_api/helper"
	"gitlab.com/midoryaga13/go_base_api/service"
)

// contract of what this controller can do
type AuthHandler interface {
	Login(ctx *gin.Context)
	Register(ctx *gin.Context)
}

type authHandler struct {
	authService service.AuthService
	jwtService  service.JWTService
	userService service.UserService
}

func NewAuthHandler(
	authService service.AuthService,
	jwtService service.JWTService,
	userService service.UserService,
) AuthHandler {
	return &authHandler{
		authService: authService,
		jwtService:  jwtService,
		userService: userService,
	}
}

func (c *authHandler) Login(ctx *gin.Context) {
	var loginRequest dto.UserLoginDto
	err := ctx.ShouldBind(&loginRequest)

	if err != nil {
		helper := helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmtyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, helper)
		return
	}

	err = c.authService.VerifyCredential(loginRequest.Email, loginRequest.Password)
	if err != nil {
		helper := helper.BuildErrorResponse("Failed to login", err.Error(), helper.EmtyObj{})
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, helper)
		return
	}

	user, _ := c.userService.FindUserByEmail(loginRequest.Email)

	token := c.jwtService.GenerateToken(strconv.FormatInt(user.ID, 10))
	user.Token = token
	helper := helper.BuildResponse(true, "OK!", user)
	ctx.JSON(http.StatusOK, helper)

}

func (c *authHandler) Register(ctx *gin.Context) {
	var registerRequest dto.UserCreateDto

	err := ctx.ShouldBind(&registerRequest)
	if err != nil {
		helper := helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmtyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, helper)
		return
	}

	user, err := c.userService.CreateUser(registerRequest)
	if err != nil {
		helper := helper.BuildErrorResponse(err.Error(), err.Error(), helper.EmtyObj{})
		ctx.AbortWithStatusJSON(http.StatusUnprocessableEntity, helper)
		return
	}

	token := c.jwtService.GenerateToken(strconv.FormatInt(user.ID, 10))
	user.Token = token
	helper := helper.BuildResponse(true, "OK!", user)
	ctx.JSON(http.StatusCreated, helper)

}
