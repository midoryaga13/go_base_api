package controller

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"gitlab.com/midoryaga13/go_base_api/dto"
	"gitlab.com/midoryaga13/go_base_api/helper"
	"gitlab.com/midoryaga13/go_base_api/service"
)

type BookHandler interface {
	All(ctx *gin.Context)
	CreateBook(ctx *gin.Context)
	UpdateBook(ctx *gin.Context)
	DeleteBook(ctx *gin.Context)
	FindOneBookByID(ctx *gin.Context)
}

type productHandler struct {
	productService service.BookService
	jwtService     service.JWTService
}

func NewBookHandler(productService service.BookService, jwtService service.JWTService) BookHandler {
	return &productHandler{
		productService: productService,
		jwtService:     jwtService,
	}
}

func (c *productHandler) All(ctx *gin.Context) {
	authHeader := ctx.GetHeader("Authorization")
	token := c.jwtService.ValidateToken(authHeader, ctx)
	claims := token.Claims.(jwt.MapClaims)
	userID := fmt.Sprintf("%v", claims["user_id"])

	products, err := c.productService.All(userID)
	if err != nil {
		helper := helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmtyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, helper)
		return
	}

	helper := helper.BuildResponse(true, "OK!", products)
	ctx.JSON(http.StatusOK, helper)
}

func (c *productHandler) CreateBook(ctx *gin.Context) {
	var createBookReq dto.BookCreateDto
	err := ctx.ShouldBind(&createBookReq)

	if err != nil {
		helper := helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmtyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, helper)
		return
	}

	authHeader := ctx.GetHeader("Authorization")
	token := c.jwtService.ValidateToken(authHeader, ctx)
	claims := token.Claims.(jwt.MapClaims)
	userID := fmt.Sprintf("%v", claims["user_id"])

	res, err := c.productService.CreateBook(createBookReq, userID)
	if err != nil {
		helper := helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmtyObj{})
		ctx.AbortWithStatusJSON(http.StatusUnprocessableEntity, helper)
		return
	}

	helper := helper.BuildResponse(true, "OK!", res)
	ctx.JSON(http.StatusCreated, helper)

}

func (c *productHandler) FindOneBookByID(ctx *gin.Context) {
	id := ctx.Param("id")

	res, err := c.productService.FindOneBookByID(id)
	if err != nil {
		helper := helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmtyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, helper)
		return
	}

	helper := helper.BuildResponse(true, "OK!", res)
	ctx.JSON(http.StatusOK, helper)
}

func (c *productHandler) DeleteBook(ctx *gin.Context) {
	id := ctx.Param("id")

	authHeader := ctx.GetHeader("Authorization")
	token := c.jwtService.ValidateToken(authHeader, ctx)
	claims := token.Claims.(jwt.MapClaims)
	userID := fmt.Sprintf("%v", claims["user_id"])

	err := c.productService.DeleteBook(id, userID)
	if err != nil {
		helper := helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmtyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, helper)
		return
	}
	helper := helper.BuildResponse(true, "OK!", helper.EmtyObj{})
	ctx.JSON(http.StatusOK, helper)
}

func (c *productHandler) UpdateBook(ctx *gin.Context) {
	updateBookRequest := dto.BookUpdateDto{}
	err := ctx.ShouldBind(&updateBookRequest)

	if err != nil {
		helper := helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmtyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, helper)
		return
	}

	authHeader := ctx.GetHeader("Authorization")
	token := c.jwtService.ValidateToken(authHeader, ctx)
	claims := token.Claims.(jwt.MapClaims)
	userID := fmt.Sprintf("%v", claims["user_id"])

	id, _ := strconv.ParseInt(ctx.Param("id"), 0, 64)
	updateBookRequest.ID = uint64(id)
	product, err := c.productService.UpdateBook(updateBookRequest, userID)
	if err != nil {
		helper := helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmtyObj{})
		ctx.AbortWithStatusJSON(http.StatusUnprocessableEntity, helper)
		return
	}

	helper := helper.BuildResponse(true, "OK!", product)
	ctx.JSON(http.StatusOK, helper)

}
