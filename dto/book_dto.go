package dto

type BookUpdateDto struct {
	ID          uint64 `json:"id" form:"id" binding:"required"`
	Title       string `json:"title" form:"title" binding:"required"`
	Price       uint64 `json:"price" form:"price" binding:"required"`
	Description string `json:"description" form:"description" binding:"required"`
	UserId      uint64 `json:"user_id,omitempty" form:"user_id" binding:"required"`
}

type BookCreateDto struct {
	Title       string `json:"title" form:"title" binding:"required"`
	Description string `json:"description" form:"description" binding:"required"`
	Price       uint64 `json:"price" form:"price" binding:"required"`
	UserId      uint64 `json:"user_id,omitempty" form:"user_id" binding:"required"`
}
