package _book

import (
	_user "gitlab.com/midoryaga13/go_base_api/helper/user"
	"gitlab.com/midoryaga13/go_base_api/models"
)

type BookResponse struct {
	ID          int64              `json:"id"`
	Title       string             `json:"title"`
	Price       uint64             `json:"price"`
	Description string             `json:"description"`
	User        _user.UserResponse `json:"user,omitempty"`
}

func NewBookResponse(book models.Book) BookResponse {
	return BookResponse{
		ID:          int64(book.ID),
		Title:       book.Title,
		Price:       book.Price,
		Description: book.Description,
		User:        _user.NewUserResponse(book.User),
	}
}

func NewBookArrayResponse(products []models.Book) []BookResponse {
	productRes := []BookResponse{}
	for _, v := range products {
		p := BookResponse{
			ID:          int64(v.ID),
			Title:       v.Title,
			Price:       v.Price,
			Description: v.Description,
			User:        _user.NewUserResponse(v.User),
		}
		productRes = append(productRes, p)
	}
	return productRes
}
