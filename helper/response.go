package helper

import "strings"

type Response struct {
	Status  bool        `json:"status"`
	Message string      `json:"message"`
	Errors  interface{} `json:"errors"`
	Data    interface{} `json:"data"`
}

type EmtyObj struct{}

func BuildResponse(status bool, message string, data interface{}) Response {
	res := Response{
		Status:  status,
		Message: message,
		Data:    data,
		Errors:  nil,
	}
	return res
}

func BuildErrorResponse(message string, errors string, data interface{}) Response {
	splittedError := strings.Split(errors, "\n")
	res := Response{
		Status:  false,
		Message: message,
		Data:    data,
		Errors:  splittedError,
	}
	return res
}
