package _user

import "gitlab.com/midoryaga13/go_base_api/models"

type UserResponse struct {
	ID    int64  `json:"id"`
	Name  string `json:"name"`
	Email string `json:"email"`
	Token string `json:"token,omitempty"`
}

func NewUserResponse(user models.User) UserResponse {
	return UserResponse{
		ID:    int64(user.ID),
		Email: user.Email,
		Name:  user.Name,
	}
}
