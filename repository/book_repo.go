package repository

import (
	"gitlab.com/midoryaga13/go_base_api/models"
	"gorm.io/gorm"
)

type BookRepository interface {
	All(userID string) ([]models.Book, error)
	InsertBook(product models.Book) (models.Book, error)
	UpdateBook(product models.Book) (models.Book, error)
	DeleteBook(productID string) error
	FindOneBookByID(ID string) (models.Book, error)
	FindAllBook(userID string) ([]models.Book, error)
}

type productRepo struct {
	connection *gorm.DB
}

func NewBookRepo(connection *gorm.DB) BookRepository {
	return &productRepo{
		connection: connection,
	}
}

func (c *productRepo) All(userID string) ([]models.Book, error) {
	products := []models.Book{}
	c.connection.Preload("User").Where("user_id = ?", userID).Find(&products)
	return products, nil
}

func (c *productRepo) InsertBook(product models.Book) (models.Book, error) {
	c.connection.Save(&product)
	c.connection.Preload("User").Find(&product)
	return product, nil
}

func (c *productRepo) UpdateBook(product models.Book) (models.Book, error) {
	c.connection.Save(&product)
	c.connection.Preload("User").Find(&product)
	return product, nil
}

func (c *productRepo) FindOneBookByID(productID string) (models.Book, error) {
	var product models.Book
	res := c.connection.Preload("User").Where("id = ?", productID).Take(&product)
	if res.Error != nil {
		return product, res.Error
	}
	return product, nil
}

func (c *productRepo) FindAllBook(userID string) ([]models.Book, error) {
	products := []models.Book{}
	c.connection.Where("user_id = ?", userID).Find(&products)
	return products, nil
}

func (c *productRepo) DeleteBook(productID string) error {
	var product models.Book
	res := c.connection.Preload("User").Where("id = ?", productID).Take(&product)
	if res.Error != nil {
		return res.Error
	}

	c.connection.Delete(&product)
	return nil
}
