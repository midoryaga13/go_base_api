package service

import (
	"errors"
	"log"

	"gitlab.com/midoryaga13/go_base_api/repository"
	"golang.org/x/crypto/bcrypt"
)

// a contract about something that this service can do
type AuthService interface {
	VerifyCredential(email string, password string) error
	// CreateUser(user dto.) entity.User
}

type authService struct {
	userrepository repository.UserRepository
}

func NewAuthService(userrepository repository.UserRepository) AuthService {
	return &authService{
		userrepository: userrepository,
	}
}

func (c *authService) VerifyCredential(email string, password string) error {
	user, err := c.userrepository.FindByEmail(email)
	if err != nil {
		println("hehe")
		println(err.Error())
		return err
	}

	isValidPassword := comparePassword(user.Password, []byte(password))
	if !isValidPassword {
		return errors.New("failed to login. check your credential")
	}

	return nil

}

func comparePassword(hashedPwd string, plainPassword []byte) bool {
	byteHash := []byte(hashedPwd)
	err := bcrypt.CompareHashAndPassword(byteHash, plainPassword)
	if err != nil {
		log.Println(err)
		return false
	}
	return true
}
