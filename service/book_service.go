package service

import (
	"errors"
	"fmt"
	"log"
	"strconv"

	"github.com/mashingan/smapping"
	"gitlab.com/midoryaga13/go_base_api/dto"
	_book "gitlab.com/midoryaga13/go_base_api/helper/book"
	"gitlab.com/midoryaga13/go_base_api/models"
	"gitlab.com/midoryaga13/go_base_api/repository"
)

type BookService interface {
	All(userID string) (*[]_book.BookResponse, error)
	CreateBook(bookDto dto.BookCreateDto, userID string) (*_book.BookResponse, error)
	UpdateBook(bookUpdateDto dto.BookUpdateDto, userID string) (*_book.BookResponse, error)
	FindOneBookByID(bookID string) (*_book.BookResponse, error)
	DeleteBook(bookID string, userID string) error
}

type productService struct {
	bookRepo repository.BookRepository
}

func NewBookService(bookRepo repository.BookRepository) BookService {
	return &productService{
		bookRepo: bookRepo,
	}
}

func (c *productService) All(userID string) (*[]_book.BookResponse, error) {
	products, err := c.bookRepo.All(userID)
	if err != nil {
		return nil, err
	}

	prods := _book.NewBookArrayResponse(products)
	return &prods, nil
}

func (c *productService) CreateBook(bookDto dto.BookCreateDto, userID string) (*_book.BookResponse, error) {
	book := models.Book{}
	err := smapping.FillStruct(&book, smapping.MapFields(&bookDto))

	if err != nil {
		log.Fatalf("Failed map %v", err)
		return nil, err
	}

	id, _ := strconv.ParseInt(userID, 0, 64)
	book.UserId = uint64(id)
	p, err := c.bookRepo.InsertBook(book)
	if err != nil {
		return nil, err
	}

	res := _book.NewBookResponse(p)
	return &res, nil
}

func (c *productService) FindOneBookByID(bookID string) (*_book.BookResponse, error) {
	book, err := c.bookRepo.FindOneBookByID(bookID)

	if err != nil {
		return nil, err
	}

	res := _book.NewBookResponse(book)
	return &res, nil
}

func (c *productService) UpdateBook(bookUpdateDto dto.BookUpdateDto, userID string) (*_book.BookResponse, error) {
	book, err := c.bookRepo.FindOneBookByID(fmt.Sprintf("%d", bookUpdateDto.ID))
	if err != nil {
		return nil, err
	}

	uid, _ := strconv.ParseInt(userID, 0, 64)
	if int64(book.UserId) != uid {
		return nil, errors.New("produk ini bukan milik anda")
	}

	book = models.Book{}
	err = smapping.FillStruct(&book, smapping.MapFields(&bookUpdateDto))

	if err != nil {
		return nil, err
	}

	book.UserId = uint64(uid)
	book, err = c.bookRepo.UpdateBook(book)

	if err != nil {
		return nil, err
	}

	res := _book.NewBookResponse(book)
	return &res, nil
}

func (c *productService) DeleteBook(bookID string, userID string) error {
	book, err := c.bookRepo.FindOneBookByID(bookID)
	if err != nil {
		return err
	}

	if fmt.Sprintf("%d", book.UserId) != userID {
		return errors.New("produk ini bukan milik anda")
	}

	c.bookRepo.DeleteBook(bookID)
	return nil

}
