package service

import (
	"errors"
	"log"

	"github.com/mashingan/smapping"
	"gitlab.com/midoryaga13/go_base_api/dto"
	_user "gitlab.com/midoryaga13/go_base_api/helper/user"
	"gitlab.com/midoryaga13/go_base_api/models"
	"gitlab.com/midoryaga13/go_base_api/repository"
	"gorm.io/gorm"
)

type UserService interface {
	CreateUser(registerRequest dto.UserCreateDto) (*_user.UserResponse, error)
	UpdateUser(updateUserRequest dto.UserUpdateDto) (*_user.UserResponse, error)
	FindUserByEmail(email string) (*_user.UserResponse, error)
	FindUserByID(userID string) (*_user.UserResponse, error)
}

type userService struct {
	userrepository repository.UserRepository
}

func NewUserService(userrepository repository.UserRepository) UserService {
	return &userService{
		userrepository: userrepository,
	}
}

func (c *userService) UpdateUser(updateUserRequest dto.UserUpdateDto) (*_user.UserResponse, error) {
	user := models.User{}
	err := smapping.FillStruct(&user, smapping.MapFields(&updateUserRequest))

	if err != nil {
		return nil, err
	}

	user, err = c.userrepository.UpdateUser(user)
	if err != nil {
		return nil, err
	}

	res := _user.NewUserResponse(user)
	return &res, nil

}

func (c *userService) CreateUser(registerRequest dto.UserCreateDto) (*_user.UserResponse, error) {
	user, err := c.userrepository.FindByEmail(registerRequest.Email)

	if err == nil {
		return nil, errors.New("user already exists")
	}

	if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, err
	}

	err = smapping.FillStruct(&user, smapping.MapFields(&registerRequest))

	if err != nil {
		log.Fatalf("Failed map %v", err)
		return nil, err
	}

	user, _ = c.userrepository.InsertUser(user)
	res := _user.NewUserResponse(user)
	return &res, nil

}

func (c *userService) FindUserByEmail(email string) (*_user.UserResponse, error) {
	user, err := c.userrepository.FindByEmail(email)

	if err != nil {
		return nil, err
	}

	userResponse := _user.NewUserResponse(user)
	return &userResponse, nil
}

func (c *userService) FindUserByID(userID string) (*_user.UserResponse, error) {
	user, err := c.userrepository.FindByUserID(userID)

	if err != nil {
		return nil, err
	}

	userResponse := _user.UserResponse{}
	err = smapping.FillStruct(&userResponse, smapping.MapFields(&user))
	if err != nil {
		return nil, err
	}
	return &userResponse, nil
}
